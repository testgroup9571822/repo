import React from 'react'
import ReactDOM from 'react-dom/client'

import App from './App.tsx'
import PromoEditor from './components/promoEditor/promoEditor.tsx'

import './index.css'

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
<>    <App />
    <PromoEditor />
 </>,
)

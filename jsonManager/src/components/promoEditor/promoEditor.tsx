import promoData from '../../assets/Promonew.json'
import PromoCard from '../promoCard/promoCard';

function PromoEditor() {
    console.log(promoData)
    return     <div id="PromoEditor">
        {
            promoData.map(e => <PromoCard promo={e} /> )
        }

    </div>;
}

export default PromoEditor;
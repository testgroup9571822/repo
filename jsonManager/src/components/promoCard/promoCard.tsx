import './promoCard.scss';
import CardInner from '../cardInner/cardInner';

interface Promo  {
promoInfo:PromoInfo
}
type PromoInfo ={
title:string,
description:string,
img:string,
video:string,
startDate:string | Date,
endDate:string |Date
}

function PromoCard(props:{ promo: Promo}) {
console.log(props)

const{title , img , description}= props.promo.promoInfo;

    return <div className="promoCard">
    <CardInner></CardInner>
</div>;
}

export default PromoCard;